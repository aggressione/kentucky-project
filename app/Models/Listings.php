<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Listings extends Model {

	protected $table = "listings";

	protected $fillable = [
		'BusinessName',
		'BusinessEmail',
		'Address',
		'Latitude',
		'Longitude',
		'MailingAddress',
		'DmoCity',
		'WebsiteSubcategory',
		'OVGCategory',
		'OVGSubCategory',
		'Phone',
		'Website',
		'BookingEmail',
		'CurrentID',
		'OldID',
		'Description',
		'google_location_name',
		'sync_direction',
		'state',
		'zip',
		'googleCategory',
		'KYViewCount',

	];
	protected $guarded = [];

	public static function getByUserId($user_id) {

		$model = Listings::where('user_id', '=', $user_id);

		return $model;
	}
}
