<?php

namespace App\Http\Middleware;

use Closure;

class googleheader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
       // $response->header('X-GOOG-API-FORMAT-VERSION', '2');
        $response->headers->set('X-GOOG-API-FORMAT-VERSION', 2);
     return $response;
 }
}
