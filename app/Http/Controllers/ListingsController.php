<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Listings;
use App\Http\Controllers\GoogleController;
use Google_Service_MyBusiness_Location;
use Google_Service_MyBusiness_PostalAddress;
use Google_Service_MyBusiness_LatLng;
use Google_Service_MyBusiness_Profile;
use Google_Service_MyBusiness_Category;


class ListingsController extends Controller
{

	private $googleCntrl;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->middleware('auth');
    	$this->googleCntrl = new GoogleController();

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {		



    	$this->googleCntrl->googleAutorize(request());

    	$aData = [];

    	$userId = \Auth::user()->id;

    	$listings = Listings::getByUserId($userId);

    	$aData['listings'] = $listings->get();

    	$aData['isAutorized'] = TRUE;

    	if ($this->isTokenExpired()) {

    		$aData['gButton'] = view('partials/googleLoginButton');
    		$aData['isAutorized'] = FALSE;

    	}
    	$aData['googleLocation'] = view('partials/googleLocationsDropdown', ['locations' => $this->getGoogleLocations()]);
    	
    	return view('index', $aData);

    } 

    public function getEditListing($id){

    	$this->googleCntrl->googleAutorize(request());

    	$cats = $this->googleCntrl->getGoogleCategoriesJson();

    	if(is_null($cats)){
    			$categories = [];
    	}
    	else{
    			$categories = $cats->categories;

    	}


    	$listing = Listings::findOrFail($id);

    	return view('edit-listing', ['listing' => $listing, 'googleCategories' => $categories ]);

    }
    
    public function postEditListing(Request $request)
    {

    	$listing = Listings::findOrFail($request->get('id'));
    	
    	$data = $request->all();


    	$id = $listing->update($data);

    	return redirect()->back()->withErrors(['<b>' . $listing->BusinessName . '</b> was updated']);

    } 


    /*public function sync()
    {

    	$userId = \Auth::user()->id;

    	$aData = array();


    	if ($this->isTokenExpired()) {

    		$aData['gButton'] = view('partials/googleLoginButton');

    	}

    	if (!$this->isTokenExpired()) {

    		$listings = Listings::getByUserId($userId);
    		
    		$aData['listings'] = $listings->get();

    		$aData['googleLocation'] = view('partials/googleLocation', ['locations' => $this->getGoogleLocations()]);
    	}

    	return view('sync', $aData);

    }*/


    public function postSyncSettings(Request $request)
    {
    	
    	$listings =  $request->get('listings');
    	$messages = [];
    	foreach ($listings as $key => $l) {

    		$listing = Listings::findOrFail($l['listing_id']);
    		$listing->google_location_name = $l['google_location_name'];
    		$listing->sync_direction = $l['sync_direction'];
    		$listing->save();
    		$messages[$key] = '<b>' . $listing->BusinessName . '</b> sync settings were updated';

    		if ($request->get('_save_sync')){

    			$this->syncLocations($l['listing_id'], $l['sync_direction'], $l['google_location_name']);

    		}
	    	/*if ($request->get('_save_sync')){

	    		$this->syncLocations($listing->id, $listing->sync_direction, $listing->google_location_name);

	    	}*/

	    }



	    return redirect()->back()->withErrors($messages);
	}

	public function getGoogleLocationByName($name)
	{
		$loc = null;
		if (!is_null(session()->get('g-locations') )){
			foreach(session()->get('g-locations') as $l) {
				if ($name == $l->name) {
					$loc = $l;
					break;
				}
			}
		}
		return $loc;

	}

	public function getGoogleLocations()
	{

		if (!is_null(session()->get('g-locations') )){

			return session()->get('g-locations');

		}
		return array();

	}


	public function isTokenExpired()
	{      

		if( is_null(request()->session()->get('g-auth-token'))
			|| (request()->session()->get('g-auth-token')['created'] + request()->session()->get('g-auth-token')['expires_in']) < time())
		{
			return TRUE;
		}
		return FALSE;
	}


	private function syncLocations($listingId, $direction, $googleLocationName )
	{   

		if (!$direction || !$googleLocationName){
			return false;
		}
		$this->googleCntrl->googleAutorize(request());

		$googleLocation = $this->googleCntrl->getLocationByName($googleLocationName);

		$listing = Listings::findOrFail($listingId);

		/*Update local listing from google data*/
		if ($direction == '<=') {

			$this->updateListing($listingId, $googleLocation);

		}
		/*Update google location from local listing*/
		elseif ($direction == '=>') {

			$response = $this->updateGoogleLocation($listingId, $googleLocation);

		}


	}


	private function updateGoogleLocation($listingId, Google_Service_MyBusiness_Location $googleLocation)
	{	

		
		$listing = Listings::findOrFail(intval($listingId));
		
		$gAddress = $googleLocation->getAddress();
		$gLatLng = $googleLocation->getLatLng();
		$gProfile = new Google_Service_MyBusiness_Profile();
		$gPrimaryCategory = new Google_Service_MyBusiness_Category();

		/*if(!is_null($gLatLng)){	
			!is_null($listing->Latitude) && !is_null($gLatLng) ? $gLatLng->setLatitude((string) $listing->Latitude) : 0;
			!is_null($listing->Longitude) && !is_null($gLatLng) ? $gLatLng->setLongitude((string) $listing->Longitude) : 0;
		}*/

		if(!is_null($gAddress)){	
			!is_null($listing->Address) ? $gAddress->setAddressLines([(string) $listing->Address]) : 0;
			!is_null($listing->DmoCity) ? $gAddress->setLocality((string) $listing->DmoCity) : 0;
			!is_null($listing->state) ? $gAddress->setAdministrativeArea((string) $listing->state) : 0;
			!is_null($listing->zip) ? $gAddress->setPostalCode((string) $listing->zip ) : 0;
		}

		!is_null($listing->BusinessName) ? $googleLocation->setLocationName((string) $listing->BusinessName) : 0;
		!is_null($listing->Phone) ? $googleLocation->setPrimaryPhone((string) $listing->Phone) : 0;
		!is_null($listing->Website) ? $googleLocation->setWebsiteUrl((string) $listing->Website) : 0;

		!is_null($listing->Description) ? $gProfile->description = (string) $listing->Description : 0;
		
		!is_null($listing->googleCategory) ? $gPrimaryCategory->setCategoryId( (string) $listing->googleCategory ) : 0;
		

		$googleLocation->setProfile($gProfile);
		$googleLocation->setPrimaryCategory($gPrimaryCategory);
		
		$result = $this->googleCntrl->updateGoogleLocation($googleLocation);

		return redirect()->back()->with($result);
	}


	private function updateListing($listingId, Google_Service_MyBusiness_Location $googleLocation)
	{	


		$address = $googleLocation->getAddress();


		
		$aData = [];

		if(!is_null($googleLocation->getPrimaryCategory())){

			$aData['googleCategory'] = $googleLocation->getPrimaryCategory()->categoryId;

		};

		if(!is_null($googleLocation->getLocationName())){

			$aData['BusinessName'] = $googleLocation->getLocationName();

		};
		if(!is_null($address->addressLines[0])){

			$aData['Address'] = $address->addressLines[0];

		};	

		if(!is_null($googleLocation->getLatlng()) && !is_null($googleLocation->getLatlng()->latitude)){

			$aData['Latitude'] = $googleLocation->getLatlng()->latitude;

		};
		if(!is_null($googleLocation->getLatlng()) && !is_null($googleLocation->getLatlng()->longitude)){

			$aData['Longitude'] = $googleLocation->getLatlng()->longitude;

		};
		if(!is_null($address->locality)){

			$aData['DmoCity'] = $address->locality;

		};
		if(!is_null($googleLocation->getPrimaryPhone())){

			$aData['Phone'] = $googleLocation->getPrimaryPhone();	

		};
		if(!is_null($googleLocation->getWebsiteUrl())){

			$aData['Website'] = $googleLocation->getWebsiteUrl();

		};
		if(!is_null($address->administrativeArea)){

			$aData['state'] = $address->administrativeArea;

		};
		if(!is_null($address->postalCode)){

			$aData['zip'] = $address->postalCode;

		};

		if(!is_null($googleLocation->getProfile()) && !is_null($googleLocation->getProfile()->description)){

			$aData['Description'] = $googleLocation->getProfile()->description;

		};
		$listing = Listings::findOrFail($listingId);
		$listing->update($aData);

	}

}
