<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Listings;
use Google_Service_MyBusiness;
use Google_Service_MyBusiness_Account;
use Google_Service_MyBusiness_Location;
use Google_Service_Exception;
use Google_Service_MyBusiness_ReportLocationInsightsRequest;
use Google_Service_MyBusiness_BasicMetricsRequest;
use Google_Service_MyBusiness_DrivingDirectionMetricsRequest;
use Google_Service_MyBusiness_TimeRange;
use Illuminate\Support\Facades\Auth;

class GoogleController extends Controller
{


	protected $gClient;

	protected $fieldsMask = array(
		'address.addressLines' => 'Address',
		'address.administrativeArea' => 'State',
		'address.postalCode' => 'Zip',
		'address.locality' => 'City',
		//'latlng.lat' => 'Latitude',
		//'latlng.lng' => 'Longitude',
		'locationName' => 'BusinessName',
		'primaryPhone' => 'Phone',
		'websiteUrl' => 'Website',
		'profile.description' => 'Description',
		'primaryCategory' => 'Category',
	);

	protected $fieldsMaskErrors = array(
		'address.addressLines' => 'Address is not valid',
		'address.administrativeArea' => 'State must be two letter abbreviation.',
		'address.postalCode' => 'Zip Code is not valid',
		'address.locality' => 'City is not valid',
		//'latlng.lat' => 'Latitude',
		//'latlng.lng' => 'Longitude',
		'locationName' => 'BusinessName is required.',
		'primaryPhone' => 'Phone is not valid',
		'websiteUrl' => 'Website is not valid',
		'profile.description' => 'Description is not valid',
		'primaryCategory' => 'Category is not valid',
	);

	private $errors = [];
	private $success = [];

	
	public function savedToken()  {

		$this->user = Auth::user(); 


		if(session()->has('g-auth-token')){

			return session()->get('g-auth-token');
		}
		else{
			session()->put('g-auth-token', unserialize($this->user->google_token));
			return unserialize($this->user->google_token);
		}

	}

	private $user = NULL;

	public function __construct()
	{
		
		$google_redirect_url = (\Request::secure() ? 'https://' : 'http://') . config('app.domain') . config('google.redirect_uri');

		
		$this->gClient = new \Google_Client();

		
		$this->gClient->setApplicationName(config('google.application_name'));
		
		$this->gClient->setClientId(config('google.client_id'));
		
		$this->gClient->setClientSecret(config('google.client_secret'));
		
		$this->gClient->setRedirectUri($google_redirect_url);
		
		$this->gClient->setDeveloperKey(config('google.api_key'));
		
		$this->gClient->setAccessType(config('google.access_type'));

		$this->gClient->setApprovalPrompt(config('google.approval_prompt'));

		$this->gClient->setScopes(config('google.scopes'));

	}

	public function revokAccess()  {

		session()->forget('g-auth-token');
		session()->forget('g-account');
		
		$user = Auth::user();

		$user->google_token = NULL;

		$user->save();

		$this->gClient->revokeToken();
		
		return redirect()->back();

	}

	/*public function loginwithanotherProfile()  {

		$google_oauthV2 = new \Google_Service_Oauth2($this->gClient);

		$this->gClient->revokeToken();
		
		session()->flush();

		$authUrl = $this->gClient->createAuthUrl();

		return redirect()->to($authUrl);

	}*/

	public function logout()  {

		$google_oauthV2 = new \Google_Service_Oauth2($this->gClient);

		$this->gClient->revokeToken();

		session()->flush();

		return false;

	}



	public function googleAutorize(Request $request)  {

		
		$google_oauthV2 = new \Google_Service_Oauth2($this->gClient);
		
		if ($request->get('code')){

			$this->gClient->authenticate($request->get('code'));

			$token = $this->gClient->getAccessToken();

			$request->session()->put('g-auth-token', $token);

			$user = Auth::user();
			$user->google_token = serialize($token);
			

			
		}
		if ($this->savedToken()){

			$this->gClient->setAccessToken($this->savedToken());


			if ($this->gClient->isAccessTokenExpired()) {
				
				$refreshTokenSaved = $this->gClient->getRefreshToken();
				$aToken = array_merge($this->savedToken(), $this->gClient->fetchAccessTokenWithRefreshToken($refreshTokenSaved));
				$this->gClient->setAccessToken($aToken);

				$request->session()->put('g-auth-token', $aToken);
				$this->user->google_token = serialize($aToken);

			}
			
		}	

		$this->user->save();

		if ($this->gClient->getAccessToken())
		{

			$account = $this->getAccount();
			
			$this->getLocationsListPerAccount($account['name']);

			return \Redirect('/');


		} else
		{
            //For Guest user, get google login url
			$authUrl = $this->gClient->createAuthUrl();

			return redirect()->to($authUrl);
		}
	}


	public function getAccount()  {

		$account = NULL;
		$gmb = new Google_Service_MyBusiness($this->gClient);

		$profile = $gmb->accounts->get('accounts');
		
		if (count($profile->accounts) < 1) {

			return NULL;
		}
		$accounts = $profile->accounts;

		session()->put('g-account', $accounts[0]);

		return $accounts[0];

	}
	
	public function getLocationsListPerAccount($name = NULL)  {

		$locations = [];

		$gmb = new Google_Service_MyBusiness($this->gClient);
		
		$locations = $gmb->accounts_locations->listAccountsLocations( $name )->locations;

		session()->put('g-locations', $locations);

		return $locations;	

	}

	public function getLocationByName($name = NULL)  {

		if (is_null($name)) {
			return;
		}

		$gmb = new Google_Service_MyBusiness($this->gClient);
		
		$location = $gmb->accounts_locations->get( $name );
		return $location;	

	}

	public function updateGoogleLocation(Google_Service_MyBusiness_Location $googleLocation = NULL)  {

		if (is_null($googleLocation)) {
			return;
		}

		$gmb = new Google_Service_MyBusiness($this->gClient);

		foreach ($this->fieldsMask as $key => $field) {
			try {
				$gmb->accounts_locations->patch(
					$googleLocation->getName(),
					$googleLocation, 
					array(
						'updateMask' => $key,
					)
				);

				$this->success[$googleLocation->getName()][] = $field;

			} catch (Google_Service_Exception $e) {

				$this->errors[$googleLocation->getName()][] = $this->fieldsMaskErrors[$key];

			}
		}
		
		return ['notSynced' => $this->errors, 'synced' => $this->success];
	}

	public function getGoogleCategoriesJson()  {
		
		$this->googleAutorize(request());

		$file = base_path('storage/app/public/google-categories.json');
			
		if(!$this->gClient->isAccessTokenExpired() && (!file_exists($file) || filemtime($file) <  ( time() - 7 * 24 * 60 * 60 ) ) ){

			if(!$this->savedToken()){
				return NULL;
			}
			
			$url = 'https://mybusiness.googleapis.com/v4/categories' .
			'?access_token='. $this->savedToken()['access_token'].
			'&region_code=us&language_code=en';

			$jsonString = file_get_contents($url);

			file_put_contents($file, stripslashes($jsonString));

			return json_decode($jsonString);

		}
		else{
			return json_decode(file_get_contents($file));

		}

		
		


	}

	public function getReports()  {

		$this->googleAutorize(request());

		$aData = [];
		
		$aData['isAutorized'] = TRUE;

		if ($this->isTokenExpired()) {

			$aData['gButton'] = view('partials/googleLoginButton');
			$aData['isAutorized'] = FALSE;

		}

		$gmb = new Google_Service_MyBusiness($this->gClient);
		$googlocations = !is_null(session()->get('g-locations') ) ? session()->get('g-locations') : [];
		$googleAccountName = !is_null(session()->get('g-account') ) ? session()->get('g-account')['name'] : '';
		$googlocationsNames = [];
		foreach($googlocations as $l){
			$googlocationsNames[] = $l->name;
		}

		$reportMetrics = array((object) [
			'metric' => 'VIEWS_MAPS',
			'options' => [
				'AGGREGATED_DAILY',
				//'AGGREGATED_TOTAL'
			],
		],
		(object) [
			'metric' => 'VIEWS_SEARCH',
			'options' => [
				'AGGREGATED_DAILY',
				//'AGGREGATED_TOTAL'
			],
		]);

		$dt = new \DateTime();
		
		$range = new Google_Service_MyBusiness_TimeRange();
		$range->setEndTime($dt->format('Y-m-d\\TH:i:s.v\\Z'));
		$range->setStartTime($dt->modify('-1 year')->format('Y-m-d\\TH:i:s.v\\Z'));
		
		$basicRequest = new Google_Service_MyBusiness_BasicMetricsRequest();
		$basicRequest->setMetricRequests($reportMetrics);
		$basicRequest->setTimeRange($range);


		$requestBody = new Google_Service_MyBusiness_ReportLocationInsightsRequest();
		$requestBody->setLocationNames($googlocationsNames);
		$requestBody->setBasicRequest($basicRequest);
		

		/*$drivingDirectionsRequest = new Google_Service_MyBusiness_DrivingDirectionMetricsRequest();
		$drivingDirectionsRequest->setNumDays('NINETY');
		$requestBody->setDrivingDirectionsRequest($drivingDirectionsRequest);*/

		try {
			$result = $gmb->accounts_locations->reportInsights(
				$googleAccountName,
				$requestBody
			);

			$aData['locations'] = [];
			$result->locationMetrics;
			//dd($aData['locations'][0]->metricValues[0]->dimensionalValues[0]->timeDimension);

			foreach ($result->locationMetrics as $k => $metric) {
				
				$localListing = Listings::where('google_location_name', '=', $metric->locationName)->first();

				/*return metrics only to connected google locations not for all*/

				if(!is_null($localListing)){

					$aData['locations'][$k]['KYViewCount'] = $localListing->KYViewCount;

					$aData['locations'][$k]['name'] = $metric->locationName;
					
					$aData['locations'][$k]['bussinessName'] = null;
					if (!is_null(session()->get('g-locations') )){
						foreach(session()->get('g-locations') as $l) {
							if ($metric->locationName == $l->name) {
								$aData['locations'][$k]['bussinessName'] = $l->locationName;
								$aData['locations'][$k]['locationObject'] = $l;
								break;
							}
						}
					}

					foreach ($metric->getMetricValues() as $i => $mValues) {
						
						$values = $mValues->getDimensionalValues();
						$metric = $mValues->getMetric();
						
						$aData['locations'][$k]['metricValues'][$metric] = [];

						foreach ($values as $j => $value) {
							
							$aData['locations'][$k]['metricValues'][$metric][$value->timeDimension->timeRange->startTime] = !is_null($value->value) ? $value->value : 0;

							$month = date('M', strtotime($value->timeDimension->timeRange->startTime));

							if (!isset($aData['locations'][$k]['metricValues'][$metric.'_MONTHLY'][$month])) {

								$aData['locations'][$k]['metricValues'][$metric.'_MONTHLY'][$month] = 0;
								
							}
							$aData['locations'][$k]['metricValues'][$metric.'_MONTHLY'][$month] += $value->value;
							
						}		

					}
					
					/*nmerge daily VIEWS_MAPS and VIEWS_SEARCH*/
					$aData['locations'][$k]['metricValues']['MERGED'] = array_merge_recursive($aData['locations'][$k]['metricValues']['VIEWS_SEARCH'], $aData['locations'][$k]['metricValues']['VIEWS_MAPS']);
					
					/*nmerge monthly VIEWS_MAPS and VIEWS_SEARCH*/
					$aData['locations'][$k]['metricValues']['MERGED_MONTHLY'] = array_merge_recursive($aData['locations'][$k]['metricValues']['VIEWS_SEARCH_MONTHLY'], $aData['locations'][$k]['metricValues']['VIEWS_MAPS_MONTHLY']);
				}

			}

			//dd($aData['locations']);

		} catch (Google_Service_Exception $e) {

			$aData['error'] = $e;
			
		}
		
		return view('reports', $aData);

	}

	public function isTokenExpired()
	{      

		if( is_null($this->savedToken())
			|| ($this->savedToken()['created'] + $this->savedToken()['expires_in']) < time())
		{
			return TRUE;
		}
		return FALSE;
	}


}
