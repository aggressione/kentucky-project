$(document).ready(function(){

	$(".btn-sync").not('.disabled').on('click', function (ev) {
		$(ev.target).parent().find('.spinner-border').show();
	})

	$(document).on('click', ".btn-sync.disabled", function (ev) {
		ev.preventDefault();
	})

	$(".change-direction .dropdown-menu li a.dmotogoogle").click(function(){
		$(this).closest('.sync-direction').find('.dir-value').html('<h5><i class="fas fa-arrow-right text-green sync-icon"></i> <b>From DMO to Google</b></h5>');
		$(this).closest('.listing-row').find('[name*=sync_direction]').val('=>')
	});

	$(".change-direction .dropdown-menu li a.googletodmo").click(function(){
		$(this).closest('.sync-direction').find('.dir-value').html('<h5><i class="fas fa-arrow-left text-green sync-icon"></i> <b>From Google to DMO</b></h5>');
		$(this).closest('.listing-row').find('[name*=sync_direction]').val('<=')
	});

	$(".change-direction .dropdown-menu li a.notsync").click(function(){
		$(this).closest('.sync-direction').find('.dir-value').html('<h5><i class="fas fa-times text-danger sync-icon"></i> <b>Do Not Sync</b></h5>');
		$(this).closest('.listing-row').find('[name*=sync_direction]').val('0')
	});


	$('.glocations-selector .loc-item[data-value] .map-modal').click(function(event) {

		event.stopPropagation();
		event.stopImmediatePropagation();
		
	})

	$('.glocations-selector .loc-item[data-value]').not('.map-modal').click(function(event) {

		event.preventDefault();
		if($(this).attr('data-value') != ''){
			$(this).closest('.google-locations-container').addClass('is-selected');
			$(this).closest('.listing-row').find('.sync-direction').addClass('is-selected');
			
			if($(this).closest('.listing-row').find('[name*=google_location_name]').val() == ''){

				$(this).closest('.listing-row').find('a.notsync').click();

			}

			$(this).closest('.listing-row').find('.sync-direction.panel-footer').removeClass('panel-footer');
		}
		else{
			/*TODO if need to clear selection*/
			/*first must add item with no data-value */
			/*$(this).closest('.listing-row').find('.sync-direction').addClass('panel-footer');
			$(this).closest('.listing-row').find('.sync-direction .dir-value').html('<h5>Select a Google Listing to set up Sync --></h5>');
			$(this).closest('.google-locations-container').removeClass('is-selected');*/
		}

		$(this).closest('.google-locations-container').find('.selected-location').html($(this).html());

		$(this).closest('.listing-row').find('[name*="google_location_name"]').val($(this).attr('data-value'));


	});

	$.each($('[name*="google_location_name"]'), function(i, e){

		$(e).closest('.listing-row').find('.loc-item[data-value="'+$(e).val()+'"]').click();

		if($(e).closest('.listing-row').find('.loc-item[data-value="'+$(e).val()+'"]').lenght < 1 ){
			$(e).val('');
		}

	});

})