@extends('layouts.app')

@section('js')
<script type="text/javascript">
	$(function() {
		$(function() {
			$('#googleCategories').selectize({
				delimiter: ',',
				persist: false,
				items: [
				"{{ $listing->googleCategory or '' }}"
				],
				create: false
			});
		});
	});
</script>

@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link" href="/">Dashboard</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="/report">Reports</a>
				</li>
			</ul>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Edit Kentucky Tourism Listing Information</h1>
			<!-- <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus possimus perspiciatis nobis unde dolores vero facilis obcaecati maxime culpa consequuntur error placeat asperiores quibusdam eligendi, itaque provident facere delectus temporibus.</p> -->
			

			@if($errors->any())
			<div class="alert alert-success">
				{!! $errors->first() !!}
			</div>
			@endif
			


			<div class="panel panel-primary">


				<div class="panel-heading">
					<h5 class="panel-title">
						{{ $listing->BusinessName or '' }}
					</h5>
				</div>

				<div class="panel-body" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
					<form method="POST" action="/listing/edit/{{ $listing->id }} ">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $listing->id }}">
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label class="text-primary mb-0">BusinessName</label>
									<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
									<input type="text" class="form-control" name="BusinessName" value="{{ $listing->BusinessName or ''}}"/>
								</div>
								<div class="form-group">
									<label class="text-primary mb-0">Phone</label>
									<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
									<input class="form-control" type="text" name="Phone" value="{{ $listing->Phone or ''}}"/>
								</div>
								<div class="form-group">
									<label class="text-primary mb-0">Address</label>
									<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
									<input class="form-control" type="text" name="Address" value="{{ $listing->Address or ''}}"/>
								</div>
								<div class="form-group">
									<label class="text-primary mb-0">DmoCity</label>
									<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
									<input class="form-control" type="text" name="DmoCity" value="{{ $listing->DmoCity or ''}}"/>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label class="text-primary mb-0">State</label>
											<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
											<input class="form-control" type="text" name="state" value="{{ $listing->state or ''}}"/>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label class="text-primary mb-0">ZIP</label>
											<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
											<input class="form-control" type="text" name="zip" value="{{ $listing->zip or ''}}"/>
										</div>
									</div>
									
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label class="text-primary mb-0">BusinessEmail</label>
									<input class="form-control" type="text" name="BusinessEmail" value="{{ $listing->BusinessEmail or ''}}"/>
								</div>
								<div class="form-group">
									<label class="text-primary mb-0">BookingEmail</label>
									<input class="form-control" type="text" name="BookingEmail" value="{{ $listing->BookingEmail or ''}}"/>
								</div>
								<div class="form-group">
									<label class="text-primary mb-0">MailingAddress</label>
									<input class="form-control" type="text" name="MailingAddress" value="{{ $listing->MailingAddress or ''}}"/>
								</div>
								<div class="form-group">
									<label class="text-primary mb-0">OVGCategory</label>
									<input class="form-control" type="text" name="OVGCategory" value="{{ $listing->OVGCategory or ''}}"/>
								</div>
								<div class="form-group">
									<label class="text-primary mb-0">OVGSubCategory</label>
									<input class="form-control" type="text" name="OVGSubCategory" value="{{ $listing->OVGSubCategory or ''}}"/>
								</div>
								
								{{-- 
								 <div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label class="text-primary mb-0">Latitude</label>
											<input class="form-control" type="text" name="Latitude" value="{{ $listing->Latitude or ''}}"/>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label class="text-primary mb-0">Longitude</label>
											<input class="form-control" type="text" name="Longitude" value="{{ $listing->Longitude or ''}}"/>
										</div>
									</div>
								</div>
								--}}
								
							</div>
							<div class="col-lg-4">

								<div class="form-group">
									<label class="text-primary mb-0">Website</label>
									<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
									<input class="form-control" type="text" name="Website" value="{{ $listing->Website or ''}}"/>
								</div>
								<div class="form-group">
									<label class="text-primary mb-0">WebsiteSubcategory</label>
									<input class="form-control" type="text" name="WebsiteSubcategory" value="{{ $listing->WebsiteSubcategory or ''}}"/>
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label class="text-primary mb-0">CurrentID</label>
											<input class="form-control" type="text" name="CurrentID" value="{{ $listing->CurrentID or ''}}"/>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label class="text-primary mb-0">OldID</label>
											<input class="form-control" type="text" name="OldID" value="{{ $listing->OldID or ''}}"/>
										</div>
									</div>
								</div>
								<div class="form-group">
										<label class="text-primary mb-0">google Category</label>
										<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
										<select name="googleCategory" id="googleCategories" class="form-control" placeholder="Start typing">
											@foreach($googleCategories as $c)
											<option></option>
											<option value="{{ $c->categoryId  or ''}}">{{ $c->displayName  or ''}}</option>
											@endforeach
										</select>
									</div>					
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label class="text-primary mb-0">Description</label>
									<i data-toggle="tooltip" data-placement="top" title="Will be synced" class="fa fa-info-circle"></i>
									<textarea class="form-control" name="Description" rows="15">{{ $listing->Description or ''}}</textarea>
								</div>
							</div>
						</div>
						<input class="btn btn-primary" type="submit" name="" value="Update Information">  OR 	<a class="btn btn-primary" href="/">Back to sync page</a><br><br>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection
