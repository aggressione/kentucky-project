@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<ul class="nav nav-tabs">
				<li class="nav-item active">
					<a class="nav-link" href="/">Dashboard</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="/report">Reports</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Review Your Kentucky Tourism Listing Information</h1>
			<p class="lead">Below are your Kentucky tourism listings on the left hand side. This information is published on our website and in our printed guides. On the right, you can connect your business listing to your Google My Business account and sync your business information so that you only have to edit it in one place. </p>
			

			@if($errors->any())
			@foreach($errors->all() as $m)
			<div class="alert alert-success">
				{!! $m !!}
			</div>
			@endforeach
			@endif
			
		
		
			<form action="/sync" method="POST">
				<div class="panel panel-primary">


					<div class="panel-heading clearfix">
						<h5 class="panel-title pull-left">
							Your Listings
						</h5>
						<div class="dropdown pull-right">
                            @if(session()->has('g-account'))
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>

                            	<img style="border-radius: 50%" src="{{   session()->get('g-account')['profilePhotoUrl'] }}" width="20"> {{   session()->get('g-account')['accountName'] }} <span class="caret"></span>
                            </a>
                            	@endif 

                            <ul class="dropdown-menu">
                                <li>
                                <a href="/googlerevoke">Disconnect</a>
                            	</li>
                        	</ul>
                    	</div>

					</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-xs-4">
								Kentucky Tourism Listings
							</div>
							<div class="col-xs-4">
								Google My Bussiness Sync Settings
							</div>
							<div class="col-xs-4">
								Connected Google Listing
							</div>
							<!-- <div class="col-xs-12"><hr></div> -->

						</div>

						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
						@if($isAutorized)
						
							@include('partials.dashboard-autorized')

						@else
							
							@include('partials.dashboard-not-autorized')

						@endif
					</div>
					@if($isAutorized)
					<div class="panel-footer text-center">
						<input class="btn btn-primary" type="submit" name="_save" value="Save Settings">
						<input class="btn-sync btn btn-primary" type="submit" name="_save_sync" value="Sync All Now">
						<div class="spinner-border text-primary" role="status" style="display: none">
							<span class="sr-only">Loading...</span>
						</div>
					</div>
					@endif
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
