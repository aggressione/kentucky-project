@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<ul class="nav nav-tabs">
				<li class="nav-item disabled">
					<a class="nav-link  active" href="#">Login</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link" href="/">Review</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link">Sync</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="#">Reports</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Sync Settings</h1>
			<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus possimus perspiciatis nobis unde dolores vero facilis obcaecati maxime culpa consequuntur error placeat asperiores quibusdam eligendi, itaque provident facere delectus temporibus.</p>
			

			{!! $gButton or '' !!}

			@if($errors->any())
			
			@foreach( $errors->all() as $m)
			<div class="alert alert-success">
				{!! $m !!}
			</div>
			@endforeach
			@endif

			@if(session()->has('synced'))
			<div class="alert alert-success">
			@foreach(session()->get('synced') as $s)
				{{ $s }}
			@endforeach
			</div>
			@endif

			@if(session()->has('notSynced'))
			@foreach(session()->get('notSynced') as $s)
			<div class="alert alert-danger" style="display: inline-block;">
				<b>{{ $s }}</b> is not valid!
			</div>
			@endforeach
			@endif
			
			@if(isset($listings))
			@foreach($listings as $k=>$l)
			<form action="/sync" method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="listing_id" value="{{ $l->id }}">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-5 text-center">
								Kentucky Bussiness Listings
							</div>
							<div class="col-sm-2">
							</div>
							<div class="col-sm-5 text-center">
								Google Bussiness Locations
							</div>
						</div>
					</div>

					<div class="panel-body">
						<div class="row">
							<div class="col-sm-5">
								<div class="alert alert-primary">
									
									<h4>{{ $l->BusinessName or ''}}</h4>
									<p>{{ $l->Address or ''}}</p>
								</div>
								<input type="hidden" name="google_location_name" value="{{ $l->google_location_name or '' }}">

							</div>
							<div class="col-sm-2 text-center">
								<!-- <span style="line-height: 2"><i class="fas fa-not-equal fa-2x"></i></span> -->
							</div>
							
							<div class="col-sm-5 google-locations-container card">
								<div class="pull-left selected-location">
									<h4>Choose Location</h4>
									<p>Choose from location from your GMB account.</p>
								</div>
								<a class="pull-right" data-toggle="dropdown" href="#" id="dropdownMenu1" style="margin-top: 11px;
								margin-bottom: 11px;"><i class="fas fa-chevron-down fa-2x" style="margin-top: 10px;"></i></a>
								{!! $googleLocation or '' !!} 
							</div>
						</div>


						<div class="">
							<div class="btn-group btn-group-toggle" data-toggle="buttons" style="width: 100%">
								<label class="col-sm-5 btn btn-default btn-sm   {{ $l->sync_direction == '=>' ? 'active' : '' }}">
									<input type="radio" name="sync_direction" value="=>" id="option1" autocomplete="off"  {{ $l->sync_direction == '=>' ? 'checked' : '' }}>From DMO to Google  <i class="fas fa-chevron-right"></i>
								</label>
								<!-- <div class="col-sm-1"></div> -->
								<label class="do-not-sync col-sm-2 btn btn-default btn-sm   {{ $l->sync_direction == '0' ? 'active' : '' }}">
									<input type="radio" name="sync_direction" value="0" id="option2" autocomplete="off"  {{ $l->sync_direction == '0' ? 'checked' : '' }}> Do Not Sync
								</label>
								<!-- <div class="col-sm-1"></div> -->
								<label class="col-sm-5 btn btn-default btn-sm  {{ $l->sync_direction == '<=' ? 'active' : '' }}">
									<input type="radio" name="sync_direction" value="<=" id="option3" autocomplete="off" {{ $l->sync_direction == '<=' ? 'checked' : '' }}><i class="fas fa-chevron-left"></i>  From Google to DMO
								</label>
							</div>
						</div> 
						
					</div>
					<div class="panel-footer">
						<input class="btn btn-primary" type="submit" name="_save" value="Save Settings">
						<input class="btn-sync btn btn-primary {{ ($l->sync_direction != '<=' && $l->sync_direction != '=>') || is_null($l->google_location_name) ? 'disabled' : '' }}" type="submit" name="_save_sync" value="Sync Now">
						<div class="spinner-border text-primary" role="status" style="display: none">
							<span class="sr-only">Loading...</span>
						</div>
					</div>
				</div>
			</form>
			@endforeach
			@endif
		</div>
	</div>
</div>
@endsection

