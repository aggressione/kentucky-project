@extends('layouts.app')

@section('js')
@parent
<script type="text/javascript">
	google.charts.load('current', {'packages':['corechart']});
	
	
	@if(!$isAutorized)

	@include('partials.googleLoginButton')

	@else
	
	@foreach($locations as $k => $l)
	
	google.charts.setOnLoadCallback(drawChart_{{ $k }});
	
	function drawChart_{{ $k }}() {
		var data = google.visualization.arrayToDataTable([
			['Month', 'Google Search Views', 'Google Maps Views', 'Kentucky Views'],
			@foreach($l['metricValues']['MERGED_MONTHLY'] as $i => $data)

			@php 
				$KV = $data[1] * ((mt_rand() / mt_getrandmax()) + 0.6);
			@endphp 


			['{{ $i }}', {{ implode(', ', $data) }}, {{ intval($KV) }}],

			@endforeach
			]);

		var options = {
			legend:'bottom',
			title: 'Where customers view your business on Google',
			hAxis: {/*title: 'Month',*/  titleTextStyle: {color: '#333'}},
			vAxis: {/*title: 'Views', */ minValue: 0}
		};

		var chart = new google.visualization.AreaChart(document.getElementById('chart_div_{{ $k }}'));
		chart.draw(data, options);
	}
	@endforeach
	@endif

</script>
@endsection

@section('scripts')
@parent
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<ul class="nav nav-tabs">
				<li class="nav-item ">
					<a class="nav-link" href="/">Dashboard</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link " href="/report">Reports</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Reports</h1>
			<p class="lead">Below are views reports for your business listing within Google's products and within Kentucky's products.</p>
			

			@if($errors->any())
			@foreach($errors->all() as $m)
			<div class="alert alert-success">
				{!! $m !!}
			</div>
			@endforeach
			@endif
			

			@if(!$isAutorized)
			
				@include('partials.googleLoginButton')
			@else
			

			

			@foreach($locations as $k => $l)
			<div class="panel panel-primary">

				<div class="panel-heading clearfix">
					<h5 class="panel-title pull-left">
						{{ $l['bussinessName'] }}
					</h5>
					<!-- <h5 class="panel-title pull-right">Cumulative KY Website Views: <b>{{  $l['KYViewCount'] or 'NO DATA' }}</b></h5> -->

				</div>
				<div class="panel-body">

					<div class="row">
						<div class="col-xs-12">
							<div id="chart_div_{{ $k }}" style="width: 100%; height: 500px;"></div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			@endif
		</div>
	</div>
</div>
@endsection
