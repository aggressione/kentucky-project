<div class="dropdown  change-direction"  >
  <a class="btn pull-right btn-primary btn-sm"  data-toggle="dropdown" href="#" id="dropdownMenu1">Change</a>
  <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
    <li><a class="dropdown-item dmotogoogle" href="#"><h5><i class="fas fa-arrow-right text-green sync-icon"></i> <b>From DMO to Google</b></h5></a></li>
    <li><a class="dropdown-item notsync" href="#"><h5><i class="fas fa-times text-danger sync-icon"></i> <b>Do Not Sync</b></h5></a></li>
    <li><a class="dropdown-item googletodmo" href="#"><h5><i class="fas fa-arrow-left text-green sync-icon"></i> <b>From Google to DMO</b></h5></a></li>
  </ul>
</div>