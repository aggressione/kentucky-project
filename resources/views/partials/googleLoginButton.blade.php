	<div class="panel panel-footer text-center" style="border-top: 0">
		<!-- <div class="panel-heading">
			<h5 class="panel-title">
				Connect Your Google My Business Account
			</h5>
		</div> -->

		<div class="panel-body">
			<br><br>
			<a class="btn btn-primary" href="{{ url('/') }}/login/google">
				<i class="fab fa-google"></i> Connect Google Account
				<!-- <img style="width: 270px;" src="/images/btn_sign_in_google.png" > -->
			</a>
			<br><br>
			<p>
				Connect your Google My Business account to sync information between your Kentucky business listing and your business profile on Google. 
			</p>
		</div>
	</div>