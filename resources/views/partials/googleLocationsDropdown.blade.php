<ul class="dropdown-menu glocations-selector" role="menu" aria-labelledby="dropdownMenu1" style="_width: 90%">
	@foreach($locations as $k => $l)
	<li>
		<span class="loc-item" data-value={{ $l->name}}>
			<h5><b>{{ $l->locationName or '' }}</b></h5>
			<p>{{ $l->address->addressLines[0]  or '' }}<br>
			<a class="map-modal" href="javascript:void(0)" onclick="window.open('{{ $l->metadata->mapsUrl}}', 'test', 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=800,height=500,left=0,top=100')">
				<small>View Location on Google Maps</small>
			</a>
			</p>
		</span>
	</li>


	@endforeach
</ul>