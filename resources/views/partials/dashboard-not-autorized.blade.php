<div class="row">
	<div class="col-xs-4">
		@foreach($listings as $k=>$l)
		<h5><b>{{ $l->BusinessName or ''}}</b></h5>
		<p>{{ $l->Address or ''}} <br>
			<a href="listing/edit/{{ $l->id }}"><small>View/Edit Listing Information</small></a>
		</p>
		
		<input type="hidden" name="google_location_name" value="{{ $l->google_location_name or '' }}">
		
		<br>
		@endforeach
	</div>
	<div class="col-xs-8">
		
		{!!  $gButton or '' !!}
		
	</div>
</div>