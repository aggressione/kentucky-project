@foreach($listings as $k=>$l)

		<div class="row">
	<div class="col-sm-12">
		<hr>

		@if(session()->has('synced') && isset(session()->get('synced')[$l->google_location_name]))
		<div class="alert alert-success" style="display: inline-block; padding: 0 6px">
			<b>Successfully updated:</b>
			@foreach(session()->get('synced')[$l->google_location_name] as $s)
			{{ $s }}
			@endforeach
		</div>
		@endif

		@if(session()->has('notSynced') && isset(session()->get('notSynced')[$l->google_location_name]))
		@foreach(session()->get('notSynced')[$l->google_location_name] as $s)
		<div class="alert alert-danger" style="display: inline-block; padding: 0 6px">
			<b>{{ $s }}</b> is not valid!
		</div>
		@endforeach
		@endif
	</div>
</div>

<div class="row listing-row">
	<div class="col-xs-4">
		<h5><b>{{ $l->BusinessName or ''}}</b></h5>
		<p>{{ $l->Address or ''}} <br>
			<a href="listing/edit/{{ $l->id }}"><small>View/Edit Listing Information</small></a>
		</p>

		<input type="hidden" name="listings[{{ $k }}][listing_id]" value="{{ $l->id }}">
		<input type="hidden" name="listings[{{ $k }}][google_location_name]" value="{{ $l->google_location_name or '' }}">
		<input type="hidden" name="listings[{{ $k }}][sync_direction]" value="{{ $l->sync_direction or 0 }}">

		
	</div>

	@if(is_null($l->google_location_name) || $l->google_location_name == '')

	<div class="col-xs-4 panel-default panel panel-footer sync-direction">

		
		<div class="dir-value">
			<h5>Select a Google Listing to set up Sync --></h5>
		</div>
		
		
		@include('partials.sync-change-dropdown')

	</div>

	@elseif($l->sync_direction == '=>')

	<div class="col-xs-4 panel-default panel sync-direction">

		
		<div class="dir-value">
			<h5><i class="fas fa-arrow-right text-green sync-icon"></i> <b>From DMO to Google</b></h5>
		</div>
		
		
		@include('partials.sync-change-dropdown')

	</div>

	@elseif($l->sync_direction == '<=')

	<div class="col-xs-4 panel-default panel sync-direction">

		
		<div class="dir-value">
			<h5><i class="fas fa-arrow-left text-green sync-icon"></i> <b>From Google to DMO</b></h5>
		</div>
		
		
		@include('partials.sync-change-dropdown')
	</div>

	@else
	<div class="col-xs-4 panel-default panel sync-direction">
		
		<div class="dir-value">
			<h5><i class="fas fa-times text-danger sync-icon"></i> <b>Do Not Sync</b></h5>
		</div>
		
		@include('partials.sync-change-dropdown')
	</div>
	
	@endif

	<div class="col-xs-4">
		<div class="google-locations-container card panel panel-body panel-primary {{ is_null($l->google_location_name) || $l->google_location_name == '' ? '' : 'is-selected' }}">
			<div class="pull-left selected-location">
				
				<h5><b>Choose Location</b></h5>
				
			</div>
			<a class="pull-right" data-toggle="dropdown" href="#" id="dropdownMenu1"><i class="fas fa-chevron-down fa-2x" style="margin-top: 10px;"></i></a>
			<a class="pull-right" data-toggle="dropdown" href="#" id="dropdownMenu1"><i class="far fa-check-circle text-green fa-2x" style="margin-top: 10px;"></i></a>
			{!! $googleLocation or '' !!} 
		</div>
	</div>
</div>
@endforeach