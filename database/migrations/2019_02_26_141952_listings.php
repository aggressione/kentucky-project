<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Listings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('listings', function(Blueprint $table)
    	{
    		$table->increments('id');
    		$table->integer('user_id');
    		$table->string('BusinessName');
    		$table->string('Address');
    		$table->double('Latitude', 10, 2);
    		$table->double('Longitude', 10, 2);
    		$table->string('MailingAddress')->nullable();
    		$table->string('DmoCity');
    		$table->string('WebsiteSubcategory');
    		$table->string('OVGCategory')->nullable();
    		$table->string('OVGSubCategory')->nullable();
    		$table->string('Phone'); 
    		$table->string('Website')->nullable();
    		$table->string('BusinessEmail')->nullable(); 
    		$table->string('BookingEmail')->nullable();
    		$table->integer('CurrentID');
    		$table->integer('OldID');
            $table->text('Description')->nullable();
            $table->string('googleCategory')->nullable();
    		$table->string('KYViewCount')->nullable();
    		
    		$table->string('google_location_name')->nullable();
			$table->string('sync_direction')->nullable();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
