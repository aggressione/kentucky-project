<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'ListingsController@index');
Route::get('listing/edit/{id}', 'ListingsController@getEditListing');
Route::post('listing/edit/{id}', 'ListingsController@postEditListing');
Route::post('sync', 'ListingsController@postSyncSettings');
Route::get('login/google/callback', 'GoogleController@googleAutorize');
Route::get('login/google', 'GoogleController@googleAutorize');
Route::get('report', 'GoogleController@getReports');
Route::get('googlerevoke', 'GoogleController@revokAccess');